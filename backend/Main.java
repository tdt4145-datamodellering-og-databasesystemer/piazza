package backend;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main {
    Scanner sc = new Scanner(System.in);
    String email = "";  // Username in controller
    String password = "";
    UserController userController = new UserController();
    int userID = 0;

    //Logger inn i systemet
    public Boolean login() {  
        System.out.println("-----Log in:-----");

        // Question the user about login info
        System.out.println("Email: ");
        this.email = this.sc.nextLine();

        System.out.println("Password: ");
        this.password = this.sc.nextLine();

        // Validation
        if (this.userController.login(email, password)) {
            System.out.println("Successfully logged in");
            System.out.println("");
            userID = userController.getUserID(email);
            return true;
        }
        else {
            System.out.println("Wrong login information");
            System.out.println("");
            return false;
        }
    }

    //Velger hvilken handling som skal utføres av brukeren
    public void pickAction() {
        System.out.println("-----Pick action:-----");
        System.out.println("Choices: (Post), (read), (comment), (search), (statistic)");

        // Question the user of action
        String choice = this.sc.nextLine();
        choice = choice.toLowerCase();

        // Handle action
        switch (choice) {
            case "post":
                this.post();
                break;
            case "read":
                this.read();
                break;
            case "comment":
                this.comment();
                break;
            case "search":
                this.search();
                break;
            case "statistic":
                this.statistic();
                break;
            default:
                System.out.println("This action does not work");
                System.out.println("Try; Post, read ...");
        }
        
    }

    //Gir ut statestikk over hvor mange post som er laget og lest av brukerene
    private void statistic() {
        System.out.println("Skriver ut statestikk");
        userController.statistics();
    }

    //Søker etter en post med et søkeord, returnerer en rekke postID
    private void search() {
        System.out.println("Skriv inn ditt søkeord");
        String searchWord = '%'+ this.sc.nextLine()+ '%';
        userController.searchForPost(searchWord);
    }

    //Oppretter en post
    public void post() {
        int folderID = -1;
        int CourseID = 1;
        System.out.println("Hvilken type innlegg er det (tag)? Trykk enter for blankt.");
        String inpTag = this.sc.nextLine();
        // if(!inpTag.equals("")){
        //     String tag = inpTag;
        // }
        System.out.println("Skriv innlegget ditt her: ");
        String inpDescription = this.sc.nextLine();
        System.out.println("Tittel på tråden: ");
        String inpTitle = this.sc.nextLine();
        System.out.println("Navn på mappe: ");
        String inpFolder = this.sc.nextLine();
        System.out.println("Skal tråden være anonym? (Y/N)");
        String inpAnon = this.sc.nextLine();
        Boolean anon = false;
        if(inpAnon.equals("Y")){
            anon = true;
        }
        while (folderID == -1){
            if(this.userController.FolderExists(inpFolder)){
                System.out.println("Denne mappen eksisterer allerede, vil du legge innlegget ditt her? (Y/N)");
                String inpFAnswer = this.sc.nextLine();
                if(inpFAnswer.equals("Y")){
                    folderID = this.userController.getFolderID(inpFolder);
                }
                else{
                    System.out.println("Navn på mappe:");
                    inpFolder = this.sc.nextLine();
                    break;
                }
            }else{
                this.userController.createFolder(inpFolder, CourseID);
                folderID = this.userController.getFolderID(inpFolder);
                System.out.println("Mappe opprettet med navn: " + inpFolder);
            }
        }
        while(userController.inFolder(inpTitle, inpFolder)){
            System.out.println("Det eksisterer allerede en tråd ved navnet " + inpTitle + " i mappen " + inpFolder);
            System.out.println("Skriv inn ett nytt tråd-navn: ");
            inpTitle = this.sc.nextLine();
        }
        this.userController.CreateThread(inpTitle, anon, folderID, CourseID);
        int threadID = this.userController.getThreadID(inpTitle);
        this.userController.CreatePost(inpTag, inpDescription, threadID, userID);
        System.out.println("Innlegg opprettet!");
    }

    //Leser en post, først kommer alle trådene opp, man må velge postID for å lese denne posten
    //ReadCounter vil oppdateres dersom denne metoden blir utført
    public void read() {
        System.out.println("-----A list of all post-----");
        System.out.println();
        List<String[]> threads = this.userController.getThreads();  // {title, folder}, ..

        // Iteratates through the threads and the post defining the thread, then prints the information.
        Iterator<String[]> it = threads.iterator();
        while (it.hasNext()) {
            String[] thread = it.next();
            System.out.println("--ThreadID " + thread[0] + "--");
            System.out.println("Title: " + thread[1]);
            System.out.println("Folder: " + thread[2]);

            // Gets the first post in the thread as a showcase
            // String[] post = this.userController.getFirstPost(Integer.parseInt(thread[0]));
            // System.out.println("Description: " + post[1]);
            // System.out.println();
        }
        System.out.println("Skriv nummeret til posten du ønsker å lese!");
        int postID = Integer.parseInt(this.sc.nextLine());
        userController.ReadPost(userID, postID);
    }
        
    //Kommenterer på en eksisterende post, oppdaterer postcount til brukeren
    public void comment(){
        System.out.println("hvilken post ønsker du å kommentere på?");
        int parentPostID = Integer.parseInt(this.sc.nextLine());
        int threadID = userController.getThreadID(parentPostID);
        String Tag = "Svar";
        System.out.println("Hva ønsker du å svare?");
        String Description = this.sc.nextLine();
        userController.Comment(Tag, Description, parentPostID, userID);
        userController.UpdateThreadPost(threadID);
    }

    //Main loop som tar seg av spørringer fra brukeren
    public static void main(String[] args) {
        Main piazza = new Main();
        System.out.println("Welcome to Piazza!");
        boolean loggedIn = false;

        while (!loggedIn) {
            loggedIn = piazza.login();
            while (true) {
                piazza.pickAction();
            }
        }
    }
}
