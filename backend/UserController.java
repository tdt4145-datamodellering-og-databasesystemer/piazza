package backend;

import java.sql.*;
import java.util.*;

public class UserController extends DBConn{

    //Logger inn en bruker, sjekker om brukeren finnes
    public Boolean login(String email, String password){
        System.out.println("Inne i login");
        try {
            System.out.println("Inne i try" + password);
            conn = connect();
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM User");
            ResultSet Password = statement.executeQuery();
            while(Password.next()) {
                if(email.equals(Password.getString(2)) && password.equals(Password.getString(4))) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    //Liker en post, BRUKES IKKE ettersom det ikke var en av brukerhistoriene
    public void like(int PostID) {
        try {
            Statement stmt = conn.createStatement();
            String updateLikes = "UPDATE Post SET likes = likes+1 WHERE ID =" + PostID;
            stmt.executeUpdate(updateLikes);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    //Leser en post, oppdaterer read count
    public void ReadPost(int userID, int postID) {
        try {
            PreparedStatement stmt = conn.prepareStatement("UPDATE User SET ReadPostCount = ReadPostCount+1 WHERE UserID = (?) ");
            stmt.setInt(1, userID);
            stmt.executeUpdate();
            PreparedStatement postInfo = conn.prepareStatement("SELECT tag, likeCount, Description, userID FROM Post Where PostID = (?)");
            postInfo.setInt(1, postID);
            ResultSet rs = postInfo.executeQuery();
            while(rs.next()){
                System.out.println("Tag: " + rs.getString(1) + ", Likes: " + rs.getInt(2) + ", Beskrivelse: " + rs.getString(3) + ", Bruker: " + getUserEmail(rs.getInt(4)));
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Oppretter en post
    public void CreatePost(String tag, String description, int threadID, int userID){
        try {
            PreparedStatement newPost = conn.prepareStatement("INSERT INTO Post(Tag, likeCount, Description, threadID, userID) Values( (?), (?), (?), (?), (?) )");
            newPost.setString(1, tag);
            newPost.setInt(2, 0);
            newPost.setString(3, description);
            newPost.setInt(4, threadID);
            newPost.setInt(5, userID);
            newPost.execute();
            updatePostCount(userID);
            UpdateThreadPost(threadID);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Sjekker om tråden finnes
    public Boolean threadExists(String threadTitle){
        try {
            Statement stmt = conn.createStatement();
            ResultSet folderNames = stmt.executeQuery("SELECT Title FROM Thread");
            while(folderNames.next()){
                if(folderNames.getString(1).equals(threadTitle)){
                    return true;
                }
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    //Sjekker om threaden er i en folder, Brukes ikke da dette ikke var behov for brukerhistoriene
    public Boolean inFolder(String title, String foldername) {
        try {
            Statement stmt = conn.createStatement();
            ResultSet folderNames = stmt.executeQuery("SELECT Title, name FROM Thread NATURAL JOIN Folder");
            while(folderNames.next()){
                if(folderNames.getString(1).equals(title) && folderNames.getString(2).equals(foldername)) {
                    return true;
                }
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    //Sjekker om en folder finnes
    public Boolean FolderExists(String name){
        try {
            Statement stmt = conn.createStatement();
            ResultSet folderNames = stmt.executeQuery("SELECT Name FROM Folder");
            while(folderNames.next()){
                if(folderNames.getString(1).equals(name)){
                    return true;
                }
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    //Sjekker om faget finnes, BRUKES IKKE
    public Boolean courseExists(String courseCode) {
        try {
            Statement stmt = conn.createStatement();
            ResultSet courseNames = stmt.executeQuery("SELECT courseCode FROM Course");
            while(courseNames.next()){
                if(courseNames.getString(1).equals(courseCode)){
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    //Lager en tråd
    public void CreateThread(String title, Boolean isAnonymus, int folderID, int courseID) {
        try {
            Statement stmt = conn.createStatement();
            PreparedStatement newThread = conn.prepareStatement("INSERT INTO Thread(Title, isAnonymous, folderID, courseID) Values( (?), (?), (?), (?) )");
            newThread.setString(1, title);
            newThread.setBoolean(2, isAnonymus);
            newThread.setInt(3, folderID);
            newThread.setInt(4, courseID);
            newThread.execute();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Lager en subfolder
    public void createSubFolder(String name, int courseID, int parentFolder) {
        try {
            Statement stmt = conn.createStatement();
            PreparedStatement newFolder = conn.prepareStatement("INSERT INTO Folder(Name, CourseID, ParentFolder) Values( (?), (?), (?) )");
            newFolder.setString(1, name);
            newFolder.setInt(2, courseID);
            newFolder.setInt(3, parentFolder);
            newFolder.execute();
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    //Oppretter en folder
    public void createFolder(String name, int courseID) {
        try {
            //Statement stmt = conn.createStatement();
            PreparedStatement newFolder = conn.prepareStatement("INSERT INTO Folder(Name, CourseID) Values( (?), (?) )");
            newFolder.setString(1, name);
            newFolder.setInt(2, courseID);
            
            newFolder.execute();
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    //Oppretter et course
    public void createCourse(String courseCode, String name, String term){
        try {
            Statement stmt = conn.createStatement();
            PreparedStatement newCourse = conn.prepareStatement("INSERT INTO Course(CourseCode, name, term) VALUES( (?), (?), (?) )");
            newCourse.setString(1, courseCode);
            newCourse.setString(2, name);
            newCourse.setString(3, term);
            newCourse.execute();
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }


    //Kommenterer på en post ved å bruke posten som skal kommenteres på 
    public void Comment(String Tag, String Description, int parentPostID, int userID){
        try {
            PreparedStatement parentThread = conn.prepareStatement("SELECT ThreadID FROM Post WHERE postID = (?)");
            parentThread.setInt(1, parentPostID);
            ResultSet rs = parentThread.executeQuery();
            rs.next();
            int ThreadID = rs.getInt(1);
            PreparedStatement newPost = conn.prepareStatement("INSERT INTO Post(Tag, likeCount, description, parentpost, threadID,  userID) VALUES( (?), (?), (?), (?), (?), (?) )");
            newPost.setString(1, Tag);
            newPost.setInt(2, 0);
            newPost.setString(3, Description);
            newPost.setInt(4, parentPostID);
            newPost.setInt(5, ThreadID);
            newPost.setInt(6, userID);
            newPost.execute();
            updatePostCount(userID);
            UpdateThreadPost(ThreadID);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    //Oppdaterer postcount dersom noen svarer på en post
    public void UpdateThreadPost(int ThreadID){
        try {
            PreparedStatement stmt = conn.prepareStatement("UPDATE Thread SET PostCount=PostCount+1 WHERE ThreadID = (?)");
            stmt.setInt(1, ThreadID);
            stmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    //Oppdaterer hvor mange posts en bruker har laget
    public void updatePostCount(int userID) {
        try {
            PreparedStatement stmt = conn.prepareStatement("UPDATE User SET PostCount=PostCount+1 WHERE UserID = (?)");
            stmt.setInt(1, userID);
            stmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
        
    }
    
    //Søke etter post med nøkkelord, gir en "liste" med id på posts som inneholder søkeord
    public void searchForPost(String searchWord) {
        try {
            PreparedStatement matching = conn.prepareStatement("SELECT postID FROM Post WHERE description LIKE (?) ");
            matching.setString(1, searchWord);
            ResultSet rs = matching.executeQuery();
            System.out.println("ID på posts som inneholder søkeord");
            while(rs.next()){
                System.out.println(rs.getInt(1));
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    //Skriver ut statestikk over brukerene, hvor mange posts de har lest og antall de har postet
    public void statistics(){
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT Email, ReadPostCount, PostCount FROM User ORDER BY ReadPostCount DESC");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println("Bruker: " + rs.getString(1) + ", Antall lest: " + rs.getInt(2) + ", Antall posts: " + rs.getInt(3));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    //Henter ID til en bruker ved email
    public int getUserID(String email){
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT UserID FROM user Where email = (?) ");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
        
    }

    //Henter email til en bruker ved hjelp av id
    public String getUserEmail(int userID){
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT email FROM user Where userID = (?) ");
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            String email = rs.getString(1);
            return email;
        }catch(Exception e){
            e.printStackTrace();
        }
        return "Fant ikke bruker";
        
    }

    //Henter id på folder ved hjelp av tittelen til folderen
    public int getFolderID(String title){
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT FolderID FROM Folder Where Name = (?) ");
            stmt.setString(1, title);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
        
    }
    //Henter id til tråd ved tittel til tråden
    public int getThreadID(String title){
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT ThreadID FROM Thread Where Title = (?) ");
            stmt.setString(1, title);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
        
    }

    //Henter id til en tråd ved hjelp av postid
    public int getThreadID(int postID){
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT ThreadID FROM Post Where postID = (?) ");
            stmt.setInt(1, postID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
        
    }
    //Henter CourseID
    public int getCourseID(String coursecode){
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT CourseID FROM Course Where coursecode = (?) ");
            stmt.setString(1, coursecode);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
        
    }

    //Returnerer alle trådene
    public List<String[]> getThreads() {
        // Answers the action 'read' and returns: title of thread, folder of thread
        List<String[]> threads = new ArrayList<>();
        try {
            conn = connect();
            PreparedStatement stmt = conn.prepareStatement("SELECT thread.ThreadID, thread.Title, folder.Name as Folder FROM thread natural join folder");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                String[] thread = {String.valueOf(rs.getInt(1)), rs.getString(2), rs.getString(3)};
                threads.add(thread);
            }
            return threads;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Henter første post til hver tråd, IKKE I BRUK
    public String[] getFirstPost(int threadID) {
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT PostID, Description FROM piazza.post where ThreadID = ?");
            stmt.setInt(1, threadID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            String[] firstPost = {String.valueOf(rs.getInt(1)), rs.getString(2)};
            return firstPost;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    //Main metode brukt for testing underveis
    public static void main(String[] args){
        UserController uc = new UserController();
        uc.conn = uc.connect();
        System.out.println(uc.getThreadID("spm"));
        System.out.println(uc.getFolderID("Examen"));
        System.out.println(uc.FolderExists("Examen"));
        System.out.println(uc.getCourseID("TDT4141"));
        System.out.println(uc.getUserID("a"));
    }
    
}

