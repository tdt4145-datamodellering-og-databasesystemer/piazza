package backend;

import java.sql.*;
import java.util.Properties;


public abstract class DBConn {
    protected Connection conn;

    public DBConn () {
    }

    public Connection connect() {
        try {
            //The connection object for the database server
            // Class.forName("com.mysql.jdbc.Driver").getDeclaredConstructor().newInstance(); Deprecated
            Class.forName("com.mysql.cj.jdbc.Driver");
            Properties p = new Properties ();
            p.put("user","root");
            p.put("password","passord");
            conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/piazza", // ?allowPublicKeyRetrival=true&autoReconnect=true&useSSL=false",
                p); // For MySQL only
                // The format is: "jdbc:mysql://hostname:port/databaseName", "username", "password"
        } catch (Exception e) {
            e.printStackTrace();  // Unable to connect
        }
        return conn;
    }
}